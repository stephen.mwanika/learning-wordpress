<?php

/*
* Plugin Name: Hello Guest Program
* Plugin URI: http://kanzucode.com
* Description: custom plugin that returns 'Hello Guest, ...' on successful login
* Author: S.C. Mwanika
* Version: 1.0.0
*/
?>

<style>
    p {
        color: red;
        text-align: center;
        font-weight: bolder;
    }
</style>

<?php
add_action( 'init', 'my_user_check' );
function my_user_check() {
if ( is_user_logged_in() ) {
// do something because a user is logged in
echo  '<p> Hello Guest, thank you for visiting. </p>';
}
}
?>