# hooks (actions, filters, register_activation_hook, and register_deactivation_hook)

## Action Hooks
Any place an action hook, or technically a do_action() function, exists in code running on WordPress, you can insert your own code by calling the add_action() function and passing in the action hook name and your custom function with the code you want to run:
do_action( $tag, $arg );
$tag —The name of the action hook being executed.
$arg —One or more additional arguments that will get passed through to the function called from the add_action() function referencing this do_action() function. 
You can create your own hooks in a theme or plugin by adding your own do_action() functions. 

Using established hooks in the WordPress core or other plugins. 
For example, let’s say we wanted to check if a user was logged in when WordPress first loads up but before any output is displayed to the browser. We can use the init hook:

`<?php
add_action( 'init', 'my_user_check' );
function my_user_check() {
if ( is_user_logged_in() ) {
// do something because a user is logged in
}
}
?>`

In the core of WordPress, there is an action hook, do_action(init), and we are calling a function called “my_user_check” from the add_action() function. At whatever point in time the code is being executed, when it gets to the init action hook, it will then run our custom my_user_check function to do whatever we want before continuing on.

## Filters
Filters are kind of like action hooks in the sense that you can tap into them wherever
they exist in WordPress. However, instead of inserting your own code where the hook or do_action() exists, you are filtering the returned value of existing functions that are using the apply_filters() function in WordPress core, plugins, and/or themes. In other words, by utilizing filters, you can hijack content before it is inserted into the database or before it is displayed to the browser as HTML:
apply_filters( $tag, $value, $var );
$tag —The name of the filter hook.
$value —The value that the filter can be applied on.
$var —Any additional variables, such as a string or an array, passed into the filter function.
If you search the core WordPress files for apply_filters you will find that the apply_filters() function is called all over the place, and like action hooks, the apply_filters() function can also be added to and called from any theme or plugin.
Anywhere in code running on your WordPress site that you see the apply_filters() function being called, you can filter the value being returned by that function. For our example, we are going to filter the title of all posts before they are displayed to the browser. We can hook into any existing filters using the add_filter() function:

add_filter( $tag, $function, $priority, $accepted_args );
$tag—The name of the filter hook you want to filter. This should match the $tag parameter of the apply_filters() function call you want to filter the results for.
$function—The name of the custom function used to actually filter the results.
$priority—This number sets the priority in which your add_filter will run compared to other places in the code that might be referencing the same filter hook tag. By default, this value is 10.
$accepted_args—You can set the number of parameters that your custom function that handles the filtering can except. The default is 1, which is the $value parameter of the apply_filters function.
OK, so how would real code for this look? Let’s start by adding a filter to alter the title of any post returned to the browser. We know of a filter hook for the_title that looks like this:
apply_filters( 'the_title', $title, $id );
$title is the title of the post and $id is the ID of the post:

`<?php
add_filter( 'the_title', 'my_filtered_title', 10, 2 );
function my_filtered_title( $value, $id ) {
$value = '[' . $value . ']';
return $value;
}
?>`

The preceding code should wrap any post titles in brackets. If your post title was “hello world,” it would now read “[hello world].” Note that we didn’t use the $id in our custom function. If we wanted to, we could have only applied the brackets to specific post IDs.
While add_action() is meant to be used with do_action() hooks and add_filter() is meant to be used with apply_filters() hooks, the functions work the same way and are interchangeable. For readability,it is still a good idea to use the proper function depending on whether you intend to return a filtered result or just perform some code at a specific time.

## register_activation_hook
### Creating Custom Roles and Capabilities
If you have different classes of users and need to restrict what they are doing in new ways, adding custom roles and capabilities is the way to do it.
Teachers are just Authors and Students are just Subscribers.
We need a custom role for office managers who can manage users but cannot edit content, themes, options, plugins, or the general WordPress settings. We can setup the Office Manager role like so:

`function sp_roles_and_caps() {
// Office Manager Role
remove_role('office'); // in case we updated the caps below
add_role( 'office', 'Office Manager', array(
'read' => true,
'create_users' => true,
'delete_users' => true,
'edit_users' => true,
'list_users' => true,
'promote_users' => true,
'remove_users' => true,
'office_report' => true // new cap for our custom report
));
}
// run this function on plugin activation
register_activation_hook( __FILE__, 'sp_roles_and_caps' );`

When the add_role() function is run, it updates the wp_user_roles option in the wp_options table, where WordPress looks to get information on roles and capabilities.
So you only want to run this function once upon activation instead of every time at runtime. That’s why we register this function using register_activation_hook().
We also run remove_role('office') at the start there, which is useful if you want to delete a role completely, but is also useful to clear out the “office” role before adding it again in case you edited the capabilities or other settings for the role. Without the remove_role() line, the add_role() line will not run since the role already exists.
The add_role() function takes three parameters: a role name, a display name, and an array of capabilities. Use the reference in the Codex to find the names of the default capabilities or look them up in the /wp-admin/includes/schema.php file of your WordPress install.
Adding new capabilities is as simple as including a new capability name in the add_role() call or using the add_cap() method on an existing role. 

### Here is an example
### showing how to get an instance of the role class and add a capability to it:

`// give admins our office_report cap to let them view that report
$role = get_role( 'administrator' );
$role ->add_cap( 'office_report' );`

Again, this code only needs to run once, which will save it in the database. Put code like this inside of a function registered via register_activation_hook() just like the last.

## register_deactivation_hook



### Resource for this content:
https://www.oreilly.com/library/view/building-web-apps/9781449364779/
